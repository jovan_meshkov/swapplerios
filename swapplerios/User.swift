//
//  User.swift
//  swapplerios
//
//  Created by mymac on 1/11/16.
//  Copyright (c) 2016 ma. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class User {
    
    private var _uid: String = ""
    private var _photo: UIImage = UIImage(named: "user_default")!
    private var _name: String = ""
    private var _lastName: String = ""
    private var _email: String = ""
    
    
    var uid: String {
        set {
            _uid = newValue
        }
        get {
            return _uid;
        }
    }

    var photo: UIImage! {
        set {
            if newValue != nil {
                _photo = newValue
            }
        }
        get {
            return _photo
        }
    }
    
    var name: String {
        set {
            _name = newValue
        }
        get {
            return _name
        }
    }
    
    var lastName: String {
        set {
            _lastName = newValue
        }
        get {
            return _lastName
        }
    }
    
    var email: String {
        set {
            _email = newValue
        }
        get {
            return _email
        }
    }

    init() {
    
    }
    
    init(uid: String, email: String) {
        self.uid = uid
        self.email = email
    }

}