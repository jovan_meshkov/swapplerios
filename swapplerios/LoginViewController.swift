//
//  ViewController.swift
//  swapplerios
//
//  Created by mymac on 1/10/16.
//  Copyright (c) 2016 ma. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var labelMessage: SWLabel!
    
    var activityInProgress: Bool = false {
        didSet {
            if (activityInProgress) {
                activityIndicator.startAnimating()
                self.view.userInteractionEnabled = false
            }
            else {
                activityIndicator.stopAnimating()
                self.view.userInteractionEnabled = true
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showMessage(MessageType.Nothing)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func buttonLoginTouchedUpInside(sender: UIButton) {
        activityInProgress = true
        
        showMessage(MessageType.Nothing)
        
        if (!Validator.Email(textFieldEmail.text!)) {
            
            showMessage(MessageType.Error, message: "Enter valid email address!")
            activityInProgress = false
            return
        }
        
        if (!Validator.Password(textFieldPassword.text!)) {
            
            showMessage(MessageType.Error, message: "Enter valid password!")
            activityInProgress = false
            return
        }
        
        
        AuthService.loginWithEmail(textFieldEmail.text!,
            andPassword: textFieldPassword.text!,
            completion: { errorMessage, uid in
                
                if (errorMessage == nil) {
                    LocalStorage.uid = uid
                    self.performSegueWithIdentifier("segueLoginToFeed", sender: self)
                }
                else {
                    self.showMessage(MessageType.Error, message: errorMessage!)
                }
                self.activityInProgress = false
            }
        )
        

    }
    
    
    
    func showMessage(messageType: MessageType, message: String = "") {
        
        if (messageType == .Nothing) {
            labelMessage.hidden = true
            labelMessage.text = ""
            return;
        }
        
        labelMessage.hidden = false
        
        switch (messageType) {
            case .Success:
                labelMessage.textColor = UIColor(red: 23.0/255.0, green: 104.0/255.0, blue: 2.0/255.0, alpha: 1)
                labelMessage.backgroundColor = UIColor(red: 26.0/255.0, green: 149.0/255.0, blue: 1.0/255.0, alpha: 23.0/100.0)
            case .Warning:
                labelMessage.textColor = UIColor.yellowColor()
            case .Error:
                labelMessage.textColor = UIColor(red: 126.0/255.0, green: 0, blue: 0, alpha: 1)
                labelMessage.backgroundColor = UIColor(red: 1, green: 69/255.0, blue: 7/255.0, alpha: 0.5)
            default:
                break
        }
        
       
        
        labelMessage.text = message
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "segueLoginToFeed") {
            
        }
    }
}

