//
//  AuthService.swift
//  swapplerios
//
//  Created by mymac on 1/27/16.
//  Copyright (c) 2016 ma. All rights reserved.
//

import Foundation
import Firebase
import UIKit

class AuthService : Service
{
    static var signedUser: User? = nil
    
    class func loginWithEmail
        (
        email: String,
        andPassword password: String,
        completion: (errorMessage: String?, uid: String?) -> Void
        )
    {
        Global.FirebaseRoot.authUser(
            email,
            password: password,
            withCompletionBlock:
            {
                error, authData in
                
                if (error == nil) {
                    LocalStorage.uid = authData.uid
                    
                    UserService.userById(
                        authData.uid,
                        completion: {
                            errorMessage, user in
                        
                            AuthService.signedUser = user
                        }
                    )
                    
                    print(authData, terminator: "")
                    
                    completion(errorMessage: nil, uid: authData.uid)
                }
                else {
                    completion(errorMessage: Utils.messageFromError(error),uid: nil)
                }
            }
        )
    }
}