//
//  FeedSwapItemCell.swift
//  swapplerios
//
//  Created by mymac on 1/12/16.
//  Copyright (c) 2016 ma. All rights reserved.
//

import Foundation
import UIKit

class FeedSwapItemCell : UITableViewCell {
    
    @IBOutlet weak var labelname: UILabel!
    
    @IBOutlet weak var imageViewPhoto: UIImageView!
    
    @IBOutlet weak var labelOwner: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    @IBOutlet weak var labelDescription: UILabel!
    
    var data: SwapItem? = nil {
        didSet {
            if (data != nil) {
                labelname.text = data!.name
                imageViewPhoto.image = data!.image
                labelOwner.text = "Published by \(data!.owner.name) \(data!.owner.lastName)"
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "d MMMM yyyy 'at' HH:mm"
                labelDate.text = dateFormatter.stringFromDate(data!.creationDate)
                labelDescription.text = data!.description
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
}