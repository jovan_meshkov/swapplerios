//
//  FeedViewController.swift
//  swapplerios
//
//  Created by mymac on 1/12/16.
//  Copyright (c) 2016 ma. All rights reserved.
//

import Foundation
import UIKit

class FeedViewController :
    UIViewController,
    UITableViewDelegate,
    UITableViewDataSource
{
    
    @IBOutlet weak var viewMenu: UIView!
    @IBOutlet weak var contraintViewMenuWidth: NSLayoutConstraint!
    @IBOutlet weak var tableViewSwapItems: UITableView! {
        didSet {
            tableViewSwapItems.dataSource = self
            tableViewSwapItems.delegate = self
        }
    }
    
    var swapItems: [SwapItem] = []
    
    func showMenu(yes: Bool) {
        if (yes) {
            UIView.animateWithDuration(0,
                animations: {
                    self.viewMenu.transform = CGAffineTransformMakeScale(1, 1)
                    self.menuShowed = true
            })
        }
        else {
            UIView.animateWithDuration(0,
                animations: {
                    self.viewMenu.transform = CGAffineTransformMakeScale(0, 1)
                    self.menuShowed = false
            })
        }
    }
    var menuShowed = true
    
    @IBAction func buttonMenuTouchedUpInside(sender: UIButton) {
        if (menuShowed)
        {
            showMenu(false)
        }
        else {
            showMenu(true)
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showMenu(false)
        
        SwapItemService.loadLast(2, completion: {
            swapItems in
            self.swapItems = swapItems
            self.tableViewSwapItems.reloadData()
        })
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: FeedSwapItemCell? = nil
        
        cell = tableViewSwapItems.dequeueReusableCellWithIdentifier("cellFeedSwapItem") as? FeedSwapItemCell
        if (swapItems.count > 0) {
            cell!.data = swapItems[indexPath.row]
        }
        
        return cell!
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return swapItems.count
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return nil
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
}