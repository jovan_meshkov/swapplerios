//
//  Validator.swift
//  swapplerios
//
//  Created by mymac on 1/11/16.
//  Copyright (c) 2016 ma. All rights reserved.
//

import Foundation

class Validator {
    
    class func Name(value: String) -> Bool {
        var validation: Validation = Validation()
        
        validation.required = true
        validation.format = "^[A-Z]{1}[a-z]*$"
        
        return validation.validateString(value, exhaustive: true)
    }
    
    class func Email(value: String) -> Bool {
        var validation: Validation = Validation()
        
        validation.required = true
        validation.format = "^[a-zA-Z0-9]*@[a-zA-Z0-9]*\\.[a-zA-Z0-9]*$"
        
        return validation.validateString(value, exhaustive: true)
    }
    
    class func Password(value: String) -> Bool {
        var validation: Validation = Validation()
        
        validation.required = true
        validation.format = ".*"
        
        return validation.validateString(value, exhaustive: true)
    }
}
