//
//  SWFirebase.swift
//  swappler-ios
//
//  Created by mymac on 1/10/16.
//  Copyright (c) 2016 ma. All rights reserved.
//

import Foundation
import Firebase

class Global {
    static let sharedInstance = Global()
    
    static let FirebaseRoot = Firebase(url: "https://amber-inferno-2590.firebaseio.com")
}