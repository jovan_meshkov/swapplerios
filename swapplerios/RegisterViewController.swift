//
//  ViewController.swift
//  swapplerios
//
//  Created by mymac on 1/10/16.
//  Copyright (c) 2016 ma. All rights reserved.
//

import UIKit
import Firebase

class RegisterViewController :
    UIViewController,
    UIImagePickerControllerDelegate,
    UINavigationControllerDelegate
{
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var imageViewPhoto: UIImageView!
    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var textFieldLastName: UITextField!
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldConfirmPassword: UITextField!
    
    @IBOutlet weak var labelMessage: SWLabel!
    
    var tapGestureRecognizer: UITapGestureRecognizer? = nil
    
    var activityInProgress: Bool = false {
        didSet {
            if (activityInProgress) {
                activityIndicator.startAnimating()
                self.view.userInteractionEnabled = false
            }
            else {
                activityIndicator.stopAnimating()
                self.view.userInteractionEnabled = true
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showMessage(MessageType.Nothing)
        
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: "tapGestureRecognized:")
        
        imageViewPhoto.userInteractionEnabled = true
        imageViewPhoto.tag = 2684
        imageViewPhoto.addGestureRecognizer(tapGestureRecognizer!)
        
        self.view.tag = 123456789
        self.view.addGestureRecognizer(tapGestureRecognizer!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func buttonSignUpTouchedUpInside(sender: UIButton) {
        activityInProgress = true
        
        showMessage(MessageType.Nothing)

        if (!Validator.Name(textFieldName.text!)) {
            showMessage(MessageType.Error, message: "Enter valid name!")
            activityInProgress = false
            return
        }
        
        if (!Validator.Name(textFieldLastName.text!)) {
            showMessage(MessageType.Error, message: "Enter valid last name!")
            activityInProgress = false
            return
        }
        
        if (!Validator.Email(textFieldEmail.text!)) {
            showMessage(MessageType.Error, message: "Enter valid email address!")
            activityInProgress = false
            return
        }
        
        if (!Validator.Password(textFieldPassword.text!)) {
            showMessage(MessageType.Error, message: "Enter valid password!")
            activityInProgress = false
            return
        }
        
        if (textFieldPassword.text != textFieldConfirmPassword.text) {
            showMessage(MessageType.Error, message: "Passwords do not match!")
            activityInProgress = false
            return
        }
        
        UserService.registerWithName(textFieldName.text!,
            lastName: textFieldLastName.text!,
            photo: imageViewPhoto.image!,
            email: textFieldEmail.text!,
            password: textFieldPassword.text!,
            completion: {
                errorMessage, user in
                
                    self.activityInProgress = false
                    if (errorMessage != nil) {
                        self.showMessage(MessageType.Error, message: errorMessage!)
                    }
                else
                if (user != nil)
                {
                    self.showMessage(MessageType.Success, message: "Successfully signed up!")
                }
                else {
                    self.showMessage(MessageType.Error, message: "Error happend, try again!")
                }
            }
        )
    }
    
    func showMessage(messageType: MessageType, message: String = "") {
        
        if (messageType == .Nothing) {
            labelMessage.text = ""
            labelMessage.hidden = true
            return;
        }

        labelMessage.hidden = false
        
        switch (messageType) {
        case .Success:
            labelMessage.textColor = UIColor(red: 23.0/255.0, green: 104.0/255.0, blue: 2.0/255.0, alpha: 1)
            labelMessage.backgroundColor = UIColor(red: 26.0/255.0, green: 149.0/255.0, blue: 1.0/255.0, alpha: 23.0/100.0)
        case .Warning:
            labelMessage.textColor = UIColor.yellowColor()
        case .Error:
            labelMessage.textColor = UIColor(red: 126.0/255.0, green: 0, blue: 0, alpha: 1)
            labelMessage.backgroundColor = UIColor(red: 1, green: 69/255.0, blue: 7/255.0, alpha: 0.5)
        default:
            break
        }
        
        
        
        labelMessage.text = message
    }

    func tapGestureRecognized(sender: UITapGestureRecognizer) {
        
        if (Utils.pointInView(imageViewPhoto, point: sender.locationInView(self.view))) {
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            imagePicker.delegate = self
            imagePicker.editing = false
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
        
        self.view.endEditing(true)
    }
    
    // UIImagePicker delegate methods
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        imageViewPhoto.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil)
    }

}

