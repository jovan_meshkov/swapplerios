//
//  SwapItem.swift
//  swapplerios
//
//  Created by mymac on 1/12/16.
//  Copyright (c) 2016 ma. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class SwapItem {
    
    private var _id: String = ""
    private var _image: UIImage? = nil
    private var _name: String = ""
    private var _creationDate: NSDate! = nil
    private var _owner: User! = nil
    private var _description: String = ""


    
    var id: String {
        set {
            _id = newValue
        }
        get {
            return _id
        }
    }
    
    var image: UIImage! {
        set {
            _image = newValue
        }
        get {
            return _image
        }
    }
    
    var name: String {
        set {
            _name = newValue
        }
        get {
            return _name
        }
    }
    
    var description: String! {
        set {
            _description = newValue
        }
        get {
            return _description
        }
    }
    
    var owner: User! {
        set {
            _owner = newValue!
        }
        get {
            return _owner
        }
    }
    
    var creationDate: NSDate! {
        set {
            _creationDate = newValue
        }
        get {
            return _creationDate
        }
    }
    
    init() {
        
    }
    
    init(id: String, name: String, description: String, image: UIImage, owner: User, creationDate: NSDate) {
        self.id = id
        self.name = name
        self.description = description
        self.image = image
        self.owner = owner
        self.creationDate = creationDate
    }
}