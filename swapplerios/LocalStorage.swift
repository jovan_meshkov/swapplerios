//
//  LocalStorage.swift
//  swapplerios
//
//  Created by mymac on 1/11/16.
//  Copyright (c) 2016 ma. All rights reserved.
//

import Foundation

class LocalStorage {
    private static var userDefaults = NSUserDefaults.standardUserDefaults()
    
    class var uid: String? {
        get {
            return userDefaults.valueForKey("swappler.uid") as? String;
        }
        set {
            userDefaults.setValue(newValue, forKey: "swappler.uid")
        }
    }
}