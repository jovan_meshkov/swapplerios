//
//  UserService.swift
//  swapplerios
//
//  Created by mymac on 1/12/16.
//  Copyright (c) 2016 ma. All rights reserved.
//

import Foundation
import Firebase
import UIKit

extension User {

    struct node {
        static var root = "Users"
        static var name = "name"
        static var lastName = "lastName"
        static var email = "email"
        static var photo = "photo"
    }
    
    convenience init(snapshot: FDataSnapshot) {
        self.init()
        
        self.uid = snapshot.key
        self.name = snapshot.value.valueForKey(User.node.name) as! String
        self.lastName = snapshot.value.valueForKey(User.node.lastName) as! String
        self.email = snapshot.value.valueForKey(User.node.email) as! String
        self.photo = Utils.imageFromBase64String(snapshot.value.valueForKey(User.node.photo) as! String)
    }
}

class UserService : Service
{
    class func userById
        (
            uid: String,
            completion: (errorMessage: String?, user: User?) -> Void
        )
    {
        let firebaseUsers = Global.FirebaseRoot.childByAppendingPath(User.node.root)
        let firebaseUser = firebaseUsers.childByAppendingPath(uid)
        
        firebaseUser.observeSingleEventOfType(
            .Value,
            withBlock:
            {
                snapshot in
                
                let user = User(snapshot: snapshot)
                completion(errorMessage: nil, user: user)
                
            },
            withCancelBlock:
            {
                error in
                
                completion(errorMessage: Utils.messageFromError(error), user: nil)
            }
        )
        
        
    }
    
    class func registerWithName(
        name: String,
        lastName: String,
        photo aPhoto: UIImage,
        email: String,
        password: String,
        completion: (errorMessage: String?, user: User?) -> Void)
    {
        var errorMessage: String? = nil
        var user: User? = nil
        
        // Create user in Email and Password authentication
        Global.FirebaseRoot.createUser(email, password: password, withCompletionBlock: { error in
            if (error == nil) {
                
                // Auth user
                AuthService.loginWithEmail(email, andPassword: password, completion: { errorMessage, uid in
                    // Save user
                    if (errorMessage == nil) {
                        user = User()
                        user!.uid = uid!
                        user!.photo = aPhoto
                        user!.name = name
                        user!.lastName = lastName
                        user!.email = email
                        
                        let firebaseUsers = Global.FirebaseRoot.childByAppendingPath(User.node.root)
                        let firebaseUser = firebaseUsers.childByAppendingPath(uid)
                        
                        firebaseUser.setValue([
                            User.node.name: name,
                            User.node.lastName: lastName,
                            User.node.email: email,
                            User.node.photo: Utils.base64StringFromImage(aPhoto)!
                            ]
                        )
                        
                        completion(errorMessage: nil, user: user)
                    }
                    else {
                        completion(errorMessage: Utils.messageFromError(error), user: nil)
                    }
                })
            }
            else {
                completion(errorMessage: Utils.messageFromError(error), user: nil)
            }
        })
    }
}