//
//  MessageType.swift
//  swapplerios
//
//  Created by mymac on 1/11/16.
//  Copyright (c) 2016 ma. All rights reserved.
//

import Foundation

enum MessageType {
    case Nothing
    case Success
    case Error
    case Warning
}