//
//  SwapItemService.swift
//  swapplerios
//
//  Created by mymac on 1/12/16.
//  Copyright (c) 2016 ma. All rights reserved.
//

import Foundation
import Firebase
import UIKit

extension SwapItem {
    
    struct node {
        static var root = "SwapItems"
        static var image = "image"
        static var name = "name"
        static var creationDate = "creationDate"
        static var ownerId = "ownerId"
        static var description = "description"
    }
    
    convenience init(snapshot: FDataSnapshot, completion: () -> Void) {
        self.init()
        
        self.id = snapshot.key
        self.name = snapshot.value.valueForKey(SwapItem.node.name) as! String
        self.image = Utils.imageFromBase64String(snapshot.value.valueForKey(SwapItem.node.image) as! String)
        self.creationDate = NSDate.fromTimeInterval(snapshot.value.valueForKey(SwapItem.node.creationDate) as! NSTimeInterval)
        UserService.userById(
            snapshot.value.valueForKey(SwapItem.node.ownerId) as! String,
            completion:
            {
                errorMessage, user in
                if (user != nil) {
                    self.owner = user
                }
                completion()
            }
        )
    }
}

class SwapItemService : Service {
    
    class func loadLast(count: UInt, completion: (swapItems: [SwapItem]) -> Void) {
        
        var swapItems: [SwapItem] = []
        
        let swapItemsRef = Global.FirebaseRoot.childByAppendingPath(SwapItem.node.root)
        
        swapItemsRef
            //.queryOrderedByChild("date")
            //.queryLimitedToFirst(count)
            .observeSingleEventOfType(
                .Value,
                withBlock: {
                    snapshot in
                    var i: UInt = 0
                    for child in snapshot.children {
                        swapItems.append(SwapItem(snapshot: child as! FDataSnapshot, completion: {
                            i++
                            if i == snapshot.childrenCount {
                                completion(swapItems: swapItems)
                            }
                        }))
                    }
                },
                withCancelBlock: { error in
                    print(error, terminator: "")
                }
        )
    }
    
    class func addWithImage(image: UIImage, name: String, description: String, ownerId: String) {

        let firebaseSwapItems = Global.FirebaseRoot.childByAppendingPath(SwapItem.node.root)
        let firebaseSwapItem = firebaseSwapItems.childByAutoId()
        
        firebaseSwapItem.setValue(
            [
                SwapItem.node.name: name,
                SwapItem.node.image: Utils.base64StringFromImage(image)!,
                SwapItem.node.description: description,
                SwapItem.node.creationDate: NSDate.now.timeIntervalSince1970,
                SwapItem.node.ownerId: ownerId
            ]
        )
        
    }
}