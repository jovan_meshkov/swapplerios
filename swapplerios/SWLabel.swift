//
//  UILabel.swift
//  swapplerios
//
//  Created by mymac on 1/11/16.
//  Copyright (c) 2016 ma. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class SWLabel : UILabel {
    
    @IBInspectable var paddingTop: CGFloat = 0 {
        didSet {
            layoutIfNeeded()
        }
    }
    
    @IBInspectable var paddingLeft: CGFloat = 0 {
        didSet {
            layoutIfNeeded()
        }
    }
    
    @IBInspectable var paddingBottom: CGFloat = 0 {
        didSet {
            layoutIfNeeded()
        }
    }
    
    @IBInspectable var paddingRight: CGFloat = 0 {
        didSet {
            layoutIfNeeded()
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
            self.layer.masksToBounds = true;
        }
    }
    
    override func drawTextInRect(rect: CGRect) {
        let edgeInsets: UIEdgeInsets = UIEdgeInsetsMake(paddingTop, paddingLeft, paddingBottom, paddingRight)
        super.drawTextInRect(UIEdgeInsetsInsetRect(rect, edgeInsets))
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}