//
//  NSDateExtension.swift
//  swapplerios
//
//  Created by mymac on 1/28/16.
//  Copyright (c) 2016 ma. All rights reserved.
//

import Foundation

extension NSDate {
    
    // Year
    var year: Int {
        get {
            return NSCalendar.currentCalendar().component(NSCalendarUnit.Year, fromDate: self)
        }
    }
    // Month
    var month: Int {
        get {
            return NSCalendar.currentCalendar().component(NSCalendarUnit.Month, fromDate: self)
        }
    }
    // Day
    var day: Int {
        get {
            return NSCalendar.currentCalendar().component(NSCalendarUnit.Day, fromDate: self)
        }
    }
    // Hour
    var hour: Int {
        get {
            return NSCalendar.currentCalendar().component(NSCalendarUnit.Hour, fromDate: self)
        }
    }
    // Minute
    var minute: Int {
        get {
            return NSCalendar.currentCalendar().component(NSCalendarUnit.Minute, fromDate: self)
        }
    }
    // Second
    var second: Int {
        get {
            return NSCalendar.currentCalendar().component(NSCalendarUnit.Second, fromDate: self)
        }
    }
    // Nanosecond
    var nanosecond: Int {
        get {
            return NSCalendar.currentCalendar().component(NSCalendarUnit.Nanosecond, fromDate: self)
        }
    }
    
    var toTimeInterval: NSTimeInterval {
        get {
            return self.timeIntervalSince1970
        }
    }
    
    var toString: String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd MM yyyy hh:mm:ss"
        return dateFormatter.stringFromDate(self)
    }
    
    class var now: NSDate {
        get {
            return NSDate()
        }
    }
    
    class func fromTimeInterval(timeInterval: Double) -> NSDate {
        return NSDate(timeIntervalSince1970: timeInterval)
    }
    
    class func fromString(date: String) -> NSDate {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd MM yyyy hh:mm:ss"
        return dateFormatter.dateFromString(date)!
    }
    
    func yearsFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(NSCalendarUnit.Year, fromDate: date, toDate: self, options: []).year
    }
    
    func monthsFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(NSCalendarUnit.Month, fromDate: date, toDate: self, options: []).month
    }
    
    func weeksFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(NSCalendarUnit.WeekOfYear, fromDate: date, toDate: self, options: []).weekOfYear
    }
    
    func daysFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(NSCalendarUnit.Day, fromDate: date, toDate: self, options: []).day
    }
    
    func hoursFrom(date:NSDate) -> Int{
            return NSCalendar.currentCalendar().components(NSCalendarUnit.Hour, fromDate: date, toDate: self, options: []).hour
    }
    
    func minutesFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(NSCalendarUnit.Minute, fromDate: date, toDate: self, options: []).minute
    }
    
    func secondsFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(NSCalendarUnit.Second, fromDate: date, toDate: self, options: []).second
    }
    
    func offsetFrom(date:NSDate) -> String {
        if yearsFrom(date)   > 0 { return "\(yearsFrom(date))y"   }
        if monthsFrom(date)  > 0 { return "\(monthsFrom(date))M"  }
        if weeksFrom(date)   > 0 { return "\(weeksFrom(date))w"   }
        if daysFrom(date)    > 0 { return "\(daysFrom(date))d"    }
        if hoursFrom(date)   > 0 { return "\(hoursFrom(date))h"   }
        if minutesFrom(date) > 0 { return "\(minutesFrom(date))m" }
        if secondsFrom(date) > 0 { return "\(secondsFrom(date))s" }
        return ""
    }
}