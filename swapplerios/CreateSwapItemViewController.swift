//
//  CreateSwapItemViewController.swift
//  swapplerios
//
//  Created by mymac on 1/29/16.
//  Copyright (c) 2016 ma. All rights reserved.
//

import Foundation
import UIKit

class CreateSwapItemViewController :
    UIViewController,
    UIImagePickerControllerDelegate,
    UINavigationControllerDelegate
{
    
    
    @IBOutlet weak var imageViewImage: UIImageView!
    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var textViewDescription: UITextView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var tapGestureRecognizer: UITapGestureRecognizer? = nil
    
    @IBOutlet weak var labelMessage: SWLabel!
    
    var activityInProgress: Bool = false {
        didSet {
            if (activityInProgress) {
                activityIndicator.startAnimating()
                self.view.userInteractionEnabled = false
            }
            else {
                activityIndicator.stopAnimating()
                self.view.userInteractionEnabled = true
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showMessage(MessageType.Nothing)
        
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: "tapGestureRecognized:")
        
        imageViewImage.userInteractionEnabled = true
        //imageViewPhoto.tag = 2684
        imageViewImage.addGestureRecognizer(tapGestureRecognizer!)
        
        //self.view.tag = 123456789
        self.view.addGestureRecognizer(tapGestureRecognizer!)
        
    }
    
    func tapGestureRecognized(sender: UITapGestureRecognizer) {
        
        if (Utils.pointInView(imageViewImage, point: sender.locationInView(self.view))) {
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            imagePicker.delegate = self
            imagePicker.editing = false
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
        
        self.view.endEditing(true)
    }
    
    @IBAction func buttonPublishTouchedUpInside(sender: UIButton) {
        activityInProgress = true;
        
        SwapItemService.addWithImage(imageViewImage.image!,
            name: textFieldName.text!,
            description: textViewDescription.text,
            ownerId: LocalStorage.uid!)
        
        activityInProgress = false;
        showMessage(MessageType.Success, message: "Successfully published \(textFieldName.text)")
    }
    
    func showMessage(messageType: MessageType, message: String = "") {
        
        if (messageType == .Nothing) {
            labelMessage.hidden = true
            labelMessage.text = ""
            return;
        }
        
        labelMessage.hidden = false
        
        switch (messageType) {
        case .Success:
            labelMessage.textColor = UIColor(red: 23.0/255.0, green: 104.0/255.0, blue: 2.0/255.0, alpha: 1)
            labelMessage.backgroundColor = UIColor(red: 26.0/255.0, green: 149.0/255.0, blue: 1.0/255.0, alpha: 23.0/100.0)
        case .Warning:
            labelMessage.textColor = UIColor.yellowColor()
        case .Error:
            labelMessage.textColor = UIColor(red: 126.0/255.0, green: 0, blue: 0, alpha: 1)
            labelMessage.backgroundColor = UIColor(red: 1, green: 69/255.0, blue: 7/255.0, alpha: 0.5)
        default:
            break
        }
        
        
        
        labelMessage.text = message
    }
    
    // UIImagePicker delegate methods
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        imageViewImage.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
}