//
//  Utils.swift
//  swapplerios
//
//  Created by mymac on 1/11/16.
//  Copyright (c) 2016 ma. All rights reserved.
//

import Foundation
import UIKit

class Utils {
    
    class func messageFromError(error: NSError?) -> String {
        if (error == nil) {
            return "Unknown error happend!"
        }
        
        let description: String = error!.localizedDescription
        
        let descriptionSplit: [String] = description.componentsSeparatedByString(") ")
        
        return descriptionSplit[1]
    }
    
    class func pointInView(view: UIView, point: CGPoint) -> Bool {
        return CGRectContainsPoint(view.frame, point)
    }
    
    class func imageFromBase64String(base64String: String) -> UIImage? {
        if let imageData = NSData(base64EncodedString: base64String, options: NSDataBase64DecodingOptions()) {
            return UIImage(data: imageData)
        }
        return nil
    }
    
    class func base64StringFromImage(image: UIImage) -> String? {
        if let imageData = UIImagePNGRepresentation(image) {
            return imageData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions())
        }
        return nil
    }
}